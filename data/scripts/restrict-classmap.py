#!/usr/bin/python

import sys

taxon = sys.argv[1]
fn = sys.argv[2]

try:
  taxonmapping = sys.argv[3]
except:
  taxonmapping = "/home/pbradz/data/kegg/families/all-phylum-kingdom-ids.txt"

groupdict = dict()
sys.stderr.write("loading taxa...\n")
with open(taxonmapping, 'rb') as fh:
  for line in fh:
    row = line[:-1].split("\t")
    group = row[0]
    taxa = row[2].split(',')
    groupdict[group] = taxa

if not taxon in groupdict.keys():
  raise Exception("taxon not found in taxon mapping file")

acceptabletaxa = groupdict[taxon]
sys.stderr.write("Acceptable kegg ids: %s\n" % (", ".join(acceptabletaxa)))
sys.stderr.write("processing...\n")
with open(fn, 'rb') as fh:
  for line in fh:
    row = line[:-1].split(',')
    besthit = row[3]
    keggcode = besthit.split(":")[0]
    if keggcode in acceptabletaxa:
      sys.stdout.write(line)
    else:
      continue
