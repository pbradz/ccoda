#!/usr/bin/python

# get average family length per gene family per sample

import sys
familymap =  sys.argv[1]
classmap = sys.argv[2]

total_length = dict()
counts = dict()
seqs = dict()

with open(familymap, 'rb') as fh:
  for line in fh:
    row = line[:-1].split('\t')
    fam = row[0]
    seq = row[1]
    length = int(row[2])
    seqs[seq] = (fam, length)
    total_length[fam] = 0
    counts[fam] = 0

seen_fams = set()
with open(classmap, 'rb') as fh:
  for line in fh:
    row = line[:-1].split(',')
    seq = row[3]
    (fam, length) = seqs[seq]
    seen_fams.add(fam)
    total_length[fam] += float(length)
    counts[fam] += 1


print "FAMILY\tAFL"
for fam in seen_fams:
  print "%s\t%f" % (fam, float(total_length[fam]) / counts[fam])
