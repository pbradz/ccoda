#!/usr/bin/python

import sys
import numpy as np

classmap = sys.argv[1]
totalreads = 15000000

# Get a line count for resampling
with open(classmap, 'rb') as fh:
  linecount = sum([1 for line in fh])

# Do this so that the total number of reads can also vary a little
unclassified = totalreads - linecount

indices = np.random.choice(totalreads, totalreads, replace=True)
counts = np.zeros(totalreads)
for x in np.arange(totalreads):
  counts[indices[x]] += 1

lineindex = 0
with open(classmap, 'rb') as fh:
  for line in fh:
    for lc in np.arange(counts[lineindex]):
      sys.stdout.write(line)
    lineindex += 1


		
