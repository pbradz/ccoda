#!/usr/bin/python

import sys

tt_file = sys.argv[1]
fn = sys.argv[2]

acceptabletaxa = set()
sys.stderr.write("loading taxa...\n")
with open(tt_file, 'rb') as fh:
  for line in fh:
    acceptabletaxa.add(line[:-1])

sys.stderr.write("Acceptable kegg ids: %s\n" % (", ".join(acceptabletaxa)))
sys.stderr.write("processing...\n")
with open(fn, 'rb') as fh:
  for line in fh:
    row = line[:-1].split(',')
    besthit = row[3]
    keggcode = besthit.split(":")[0]
    if keggcode in acceptabletaxa:
      sys.stdout.write(line)
    else:
      continue
