#!/usr/bin/python

import sys
familymap = sys.argv[1]
classmap = sys.argv[2]

families = dict()
seqs = dict()

with open(familymap, 'rb') as fh:
  for line in fh:
    row = line[:-1].split('\t')
    fam = row[0]
    seq = row[1]
    length = int(row[2])
    seqs[seq] = (fam, length)
    families[fam] = 0

seen_fams = set()
with open(classmap, 'rb') as fh:
  for line in fh:
    row = line[:-1].split(',')
    seq = row[3]
    (fam, length) = seqs[seq]
    seen_fams.add(fam)
    families[fam] += 1

print "FAMILY\tCOUNTS"
for fam in seen_fams:
  print "%s\t%d" % (fam, families[fam])
