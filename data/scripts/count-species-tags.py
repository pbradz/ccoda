#!/usr/bin/python

import pandas
import sys
import argparse

# for some reason the hdf5 option doesn't actually save any disk space. oh
# well.

parser = argparse.ArgumentParser()
parser.add_argument("familymap",
    help = "path to family map file giving sequence lengths",
    type = str)
parser.add_argument("classmap", 
    help = "path to classification map file",
    type = str)
parser.add_argument("--output_type", "-t", type = str,
    help = "change output type [5: hdf5, c: csv]", choices = ['5','c'])
parser.add_argument("--output", "-o", help = "output file name", type = str)
parser.add_argument("--no_sparse", "-n", help = "do not write a sparse matrix",
    action = "store_true")
args = parser.parse_args()
familymap = args.familymap
classmap = args.classmap
sparse = not args.no_sparse
outfile = args.output
type = "5"
if not (args.output_type in ['5', 'c', 'C']):
  raise ValueError("output type must be '5' or 'c'")
else:
  type = args.output_type.lower()
if (type == '5') and not sparse:
  raise ValueError("you can't write a non-sparse hdf5 matrix yet")


  
#outputSummarize = False

#if (alt_format in ['Y', 'y', 'T', 't']):
#  classmap_seq = 4
#  classmap_header = True
#  classmap_fieldsep = '\t'
#if (alt_format in ['N', 'n', 'F', 'f']):
classmap_seq = 3
classmap_header = False
classmap_fieldsep = ','

#if (out_sum in ['Y', 'y', 'T', 't']):
#  outputSummarize = True
#if (out_sum in ['N', 'n', 'F', 'f']):
#  outputSummarize = False

families = dict()
seqs = dict()

sys.stderr.write("reading seqlengths...\n")
with open(familymap, 'rb') as fh:
  for line in fh:
    row = line[:-1].split('\t')
    fam = row[0]
    seq = row[1]
    length = int(row[2])
    seqs[seq] = (fam, length)
    families[fam] = []

sys.stderr.write("reading classmap...\n")
seen_fams = set()
seen_species = set()
with open(classmap, 'rb') as fh:
  if (classmap_header):
    header = fh.readline()
  for line in fh:
    row = line[:-1].split(classmap_fieldsep)
    seq = row[classmap_seq]
    org = seq.split(':')[0]
    (fam, length) = seqs[seq]
    seen_fams.add(fam)
    families[fam].append(org)
    seen_species.add(org)

nFams = len(seen_fams)
dotUnit = int(nFams / 100)

if not type == '5':
  if outfile is not None:
    outfh = open(outfile, 'wb')
  else:
    outfh = sys.stdout

if (sparse and type == '5') or ((not sparse) and type == 'c'):
  outputDF = pandas.DataFrame(index = seen_fams, columns = seen_species)
  for (fn, fam) in enumerate(seen_fams):
    if ((fn % dotUnit) == 0): sys.stderr.write(".")
    if families.has_key(fam):
      if (len(families[fam]) > 0):
        outputSet = set(families[fam])
        for x in outputSet:
          outputDF.at[fam, x] = families[fam].count(x)
  if (type == '5'):
    outputDF = outputDF.fillna(0).to_sparse(fill_value=0, kind = 'integer')
    outputDF.to_hdf(outfile, 'counts', complib = 'zlib', complevel = 9) 
  else:
    outputDF = outputDF.fillna(0)
    outputDF.to_csv(outfh, header = True, index = True) 


if (sparse and type == 'c'):
  print >> outfh, "FAMILY\tCOUNTS"
  for (fn, fam) in enumerate(seen_fams):
    if ((fn % dotUnit) == 0): sys.stderr.write(".")
    if families.has_key(fam):
      if (len(families[fam]) > 0):
        if sparse:
          outputSet = set(families[fam])
          genomeCounts = ["%s:%d" % (x, families[fam].count(x)) for x in outputSet]
          print >> outfh, "%s\t%s" % (fam, ",".join(genomeCounts))
        else:
          print >> outfh, "%s\t%s" % (fam, ",".join(families[fam])) # legacy, never run
  sys.stderr.write("\n")

if not type == '5':
  if outfile is not None:
    outfh.close()
