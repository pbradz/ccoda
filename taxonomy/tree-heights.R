#!/usr/bin/R

### Script to load in tree files and output .csvs with tree heights/densities.

library("phytools")
library("parallel")

tipHeights <- function(tree) {
  H <- nodeHeights(tree)
  n <- length(tree$tip)
  x <- H[tree$edge[,2] %in% 1:n,2]
  names(x) <- tree$tip[tree$edge[tree$edge[,2] %in% 1:n,2]]
  return(x)
}

maxheight <- function(x) {
  if (is.list(x)) { 
    max(tipHeights(x))
  } else { 
    x
  }
}
get.n <- function(tr) {
  if (is.list(tr)) { 
    length(tr$tip.label)
  } else { 
    1
  }
}

tree.files <- list.files("./trees/")
trees <- lapply(list.files("./trees/"), function(x) ((read.newick(paste("./trees/", x, sep = "")))))
# these KEGG ids correspond to bacteria/archaea
k.ids <- scan("./restricted-kegg-ids.txt","character")
al10.any.ids <- scan("./at-least-10-any.txt","character")
al10.all.ids <- scan("./at-least-10-all.txt","character")
al10.10pct.ids <- scan("./at-least-10-10pct.txt","character")
ntrees <- length(trees)

cat("Starting cluster...\n")
cl <- makeCluster(10)
cat(" ...loading libraries...\n")
clusterEvalQ(cl, library("parallel"))
clusterEvalQ(cl, library("phytools"))
cat(" ...exporting functions and data...\n")
clusterExport(cl, c("tipHeights", "maxheight", "get.n"))
clusterExport(cl, c("trees", "ntrees", "tipHeights", "maxheight", "get.n", "k.ids"))
clusterExport(cl, "get.density")

cat("Trimming trees...\n")

tree.trim <- function(trees, trim.ids = k.ids, cluster = cl) {
  force(trim.ids)
  force(trees)
  clusterExport(cluster, "trim.ids", envir = environment())
  parLapply(cluster, 1:length(trees), function(ntr) {
    tr <- trees[[ntr]]
    keep <- sapply(strsplit(tr$tip.label, "_"), function(x) x[1]) %in% trim.ids
    if (length(which(keep)) < 5) {
      return(NA)
    }
    return(drop.tip(tr, which(!keep)))
  })
}

trimmed.trees <- tree.trim(trees, k.ids)
cat("trimming trees [any]... ")
trimmed.al10.any <- tree.trim(trees, al10.any.ids)
cat("[all]... ")
trimmed.al10.all <- tree.trim(trees, al10.all.ids)
cat("[10pct]...")
trimmed.al10.10pct <- tree.trim(trees, al10.10pct.ids)
cat("\n")
al10.list <- list(any = trimmed.al10.any, all = trimmed.al10.all, pct10 = trimmed.al10.10pct)

#trimmed.trees <- parLapply(cl, 1:ntrees, function(ntr) {
#  tr <- trees[[ntr]]
#  keep <- sapply(strsplit(tr$tip.label, "_"), function(x) x[1]) %in% k.ids
#  if (length(which(keep)) < 5) {
#    return(NA)
#  }
#  return(drop.tip(tr, which(!keep)))
#  })
#

cat("Getting tree heights...\n")
cat("  ...regular...\n")
tree.heights <- parSapply(cl, trees, maxheight)
cat("  ...trimmed...\n")
trimmed.tree.heights <- parSapply(cl, trimmed.trees, maxheight)
cat("Getting tree numbers...\n")
cat("  ...regular...\n")
tree.number <- parSapply(cl, trees, get.n)
cat("  ...trimmed...\n")
trimmed.tree.number <- parSapply(cl, trimmed.trees, get.n)


get.density <- function(x) {
  if (!is.na(x)) {
    sum(x$edge.length) / mean(tipHeights(x))
  } else {NA}
}

  

tt.densities <- sapply(trimmed.trees, get.density)
t.densities <- sapply(trees, get.density)

al10.tree.number <- lapply(al10.list, function(l) parSapply(cl, l, get.n))
al10.tree.heights <- lapply(al10.list, function(l) parSapply(cl, l, maxheight))
al10.densities <- lapply(al10.list, function(l) parSapply(cl, l, get.density))

names(tree.heights) <- tree.files
names(trimmed.tree.heights) <- tree.files
names(tree.number) <- tree.files
names(trimmed.tree.number) <- tree.files
names(tt.densities) <- tree.files
names(t.densities) <- tree.files

ko.names <- sapply(tree.files, function(x) paste0("K", sub("\\.tree", "", x)))
for (l in names(al10.densities)) { names(al10.densities[[l]]) <- tree.files }
for (l in names(al10.tree.number)) { names(al10.tree.number[[l]]) <- tree.files }
for (l in names(al10.tree.heights)) { names(al10.tree.heights[[l]]) <- tree.files }


write.table(file = "tree-heights.tab", tree.heights, sep = '\t')
write.table(file = "trimmed-tree-heights.tab", trimmed.tree.heights, sep = '\t')
write.table(file = "trimmed-tree-densities.tab", tt.tree.densities, sep = '\t')
for (l in names(al10.densities)) {
  write.table(file = paste0("al10-tree-densities-", l, ".tab"), al10.densities[[l]], sep = '\t')
}

stopCluster(cl)

all.trees <- cbind(tree.heights, trimmed.tree.heights, tree.number,
  trimmed.tree.number, t.densities, tt.densities)
colnames(all.trees) <- c("tree.heights", "trimmed.tree.heights", "tree.number",
  "trimmed.tree.number", "tree.densities",
  "trimmed.tree.densities")
write.table(file = "all-trees.tab", all.trees, sep = '\t')
