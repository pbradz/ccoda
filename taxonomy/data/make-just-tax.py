#!/usr/bin/python
import sys

with open("genome-to-abbrev.tab", 'rb') as fh:
  for line in fh:
    business_end = line[:-1].split("\t")[1]
    row = business_end.split("; ")
    if (len(row) < 2):
      print >> sys.stderr, "skipping %s" % (row)
    else:
      desc = row[1]
      subrow = row[0].split(", ")
      k_id = subrow[0]
      if (len(subrow) > 2):
        ncbi_id = subrow[2]
        print("%s\t%s\t%s" % (desc, ncbi_id, k_id))
      elif len(subrow) == 2:
        ncbi_id = subrow[1]
        print("%s\t%s\t%s" % (desc, ncbi_id, k_id))
      else:
        print >> sys.stderr, "skipping %s" % (row)

