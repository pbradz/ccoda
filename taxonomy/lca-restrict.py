#!/usr/bin/python

from ete2 import Tree
from ete2 import TreeNode
from Bio import SeqIO
from os.path import expanduser
import glob
import sys
nodes = dict()

fastas = "./seqs/*.fa"

try:
  samplelist = sys.argv[1]
except:
  samplelist = "sample.list"

try:
  prefix = sys.argv[2]
except:
  prefix = "restrict"

try:
  topnode = sys.argv[3]
except:
  topnode = ["2", "2157"] # bacteria, archaea

class gsize():
  def __init__(self, name, size):
    self.name = name
    self.sizes = [int(size)]
  def add(self, size):
    self.sizes.append(int(size))
  def avgsize(self):
    return(float(sum(self.sizes)) / len(self.sizes))

def score(head, sn):
  score = 0
  for n in head.traverse("postorder"):
    try:
      if n.count.has_key(sn):
        score += n.count[sn]
    except AttributeError:
      pass
  return(score)

def size_aware_score(head, sn):
  tsize = 0
  score = 0
  for n in head.traverse("postorder"):
    try:
      if n.count.has_key(sn):
        tsize += n.size
        score += n.count[sn]
    except AttributeError:
      pass
  return((score, tsize))


print >> sys.stderr, "reading nodes"
with open(expanduser("data/nodes.dmp"), 'rb') as nodesfh:
  for line in nodesfh:
    row = line[:-1].split("\t|\t")
    taxid = row[0]
    parentid = row[1]
    rank = row[2]
    n = TreeNode()
    n.name = taxid
    n.add_features(rank = rank)
    n.add_features(pid = parentid)
    n.add_features(count = dict())
    n.add_features(size = 500000)
    nodes[taxid] = n

NCBITree = nodes["1"]
print >> sys.stderr, "adding children"
for nk in nodes.keys():
  if nk != "1":
    # Quirk of NCBI tree format is that "1" is its own parent, leading to a
    # circular tree and infinite loops = bad
    nodes[nodes[nk].pid].add_child(nodes[nk])

  
#print(NCBITree)

# print(nodes["2"])

print >> sys.stderr, "loading genome to taxon id conversion files"
with open(expanduser("data/genome_tax.list"), 'rb') as fh:
  gt = dict()
  for line in fh:
    row = line[:-1].split("\t")
    tax = row[1].split(":")[1]
    kge = row[0].split(":")[1]
    gt[kge] = tax

with open(expanduser("data/genome-to-abbrev.tab"), 'rb') as fh:
  ag = dict()
  for line in fh:
    row = line[:-1].split("\t")
    abbrev = row[1].split(",")[0]
    kge = row[0].split(":")[1]
    ag[abbrev] = kge


with open(expanduser("data/genome-sizes.tab"), 'rb') as fh:
  sizes = dict()
  for line in fh:
    row = line[:-1].split('\t')
    if sizes.has_key(row[1]):
      sizes[row[1]].add(row[0])
    else:
      sizes[row[1]] = gsize(row[1], row[0])

for gs in sizes.keys():
  if (nodes.has_key(gs)):
    nodes[gs].size = sizes[gs].avgsize()

usablenodes = dict()
for tn in topnode:
  for n in nodes[tn].traverse():
    usablenodes[n.name] = n

print >> sys.stderr, "doing stuff"
forgs = dict()
orgfs = dict()
for f in glob.glob(fastas):
  with open(f, 'rb') as fh:
    orgs = [rec.id.split(":")[0] for rec in SeqIO.parse(fh, "fasta")]
  forgs[f] = orgs
  gts = [gt[ag[o]] for o in orgs]
  # get_common_ancestor just seems to return the root
  if (len(gts) > 1):
    lca_node = NCBITree.get_common_ancestor(gts)
  else:
    lca_node = nodes[gts[0]]
  lca = lca_node.name
  rank = lca_node.rank
  after = False
  if (rank == "no rank"):
    after = True
    rank_node = lca_node
    while (rank == "no rank") and (rank_node.name != "1"):
      #print >> sys.stderr, "   -> %s" % rank_node.name
      rank_node = nodes[rank_node.pid]
      rank = rank_node.rank
  if after:
    rank = "after " + rank
  print "%s\t%s\t%s" % (f, lca, rank)

