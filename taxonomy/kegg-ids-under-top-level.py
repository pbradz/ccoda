#!/usr/bin/python

from ete2 import Tree
from ete2 import TreeNode
import sys


class gsize():
  def __init__(self, name, size):
    self.name = name
    self.sizes = [int(size)]
  def add(self, size):
    self.sizes.append(int(size))
  def avgsize(self):
    return(float(sum(self.sizes)) / len(self.sizes))

def score(head, sn):
  score = 0
  for n in head.traverse("postorder"):
    try:
      if n.count.has_key(sn):
        score += n.count[sn]
    except AttributeError:
      pass
  return(score)

def size_aware_score(head, sn):
  tsize = 0
  score = 0
  for n in head.traverse("postorder"):
    try:
      if n.count.has_key(sn):
        tsize += n.size
        score += n.count[sn]
    except AttributeError:
      pass
  return((score, tsize))


nodes = dict()
print >> sys.stderr, "reading nodes"
with open("data/nodes.dmp", 'rb') as nodesfh:
  for line in nodesfh:
    row = line[:-1].split("\t|\t")
    taxid = row[0]
    parentid = row[1]
    rank = row[2]
    n = TreeNode()
    n.name = taxid
    n.add_features(rank = rank)
    n.add_features(pid = parentid)
    n.add_features(count = dict())
    n.add_features(size = 500000)
    nodes[taxid] = n

print >> sys.stderr, "adding children"
for nk in nodes.keys():
  nodes[nodes[nk].pid].add_child(nodes[nk])

# print(nodes["2"])

print >> sys.stderr, "loading genome to taxon id conversion files"
with open("data/just-tax.txt", 'rb') as fh:
  gt = dict()
  for line in fh:
    row = line[:-1].split("\t")
    tax = row[1]
    kge = row[2]
    gt[kge] = tax

with open("data/genome-to-abbrev.tab", 'rb') as fh:
  ag = dict()
  for line in fh:
    row = line[:-1].split("\t")
    abbrev = row[1].split(",")[0]
    kge = row[0].split(":")[1]
    ag[abbrev] = kge

# read in top level terms
with open("toplevel.txt", 'rb') as fh:
  toplevel = [line[:-1] for line in fh]

accept = set()
for tl in toplevel:
  for n in nodes[tl].traverse():
    accept.add(n.name)

#for an in accept:
#  print("%s" % an)
for keggid in gt.keys():
  if (gt[keggid] in accept):
    print(keggid)
