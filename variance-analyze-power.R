source("variance-analyze-functions.R")
source("variance-analyze-tests.R")

######### Power analysis
######### 

power.wrapper <- function(
    nsamps = c(7, 20, 35, 50, 65),
    true.pct = c(0.05, 0.1, 0.25, 0.5),
    sig.thresh = c(0.05),
    low.hi.ratio = c(1),
    size.effect = c(1, 1.5, 2, 4, 8, 16),
    n.genes = 1000,
    n.studies = 3, 
    dirname = "temp",
    savetest = F,
    cheatsize = F,
    size.method = fit.shrunk.nb,
    B = 500,
    troubleshoot = F, 
    specify.cols = NULL,
    nodes = 20,
    nb.only = F, 
    logmean.distro = c(mean = 2.94, sd = 2.23),
    logdset.fx.distro = c(mean = -0.49, sd = 0.78),
    # dataset.log.medsize.distro <- c(mean = -0.65, sd = 0.57),
    dataset.log.medsize.distro = c(mean = 0.529, sd = 0.495),
    #log.ags.distro <- c(mean = 15.0, sd = 0.15),
    log.ags.distro = c(mean = 15.0, sd = 1e-5), # very low variation
    # log.afl.distro <- c(mean = 6.07, sd = 0.70),
    log.afl.distro = c(mean = 7, sd = 0.01),    # very low variation
    skip.rarefy = T
    ) {

  n.total.tests <- length(nsamps) * length(true.pct) * length(sig.thresh) * length(size.effect)
  # means, ags, afl are modeled as lognormal distributions fit from the actual data
  distros <- list(means = logmean.distro,
    dsets = logdset.fx.distro,
    sizes = dataset.log.medsize.distro,
    ags = log.ags.distro,
    afl = log.afl.distro)
  total.tests <- vector("list", n.total.tests)
  param.list <- vector("list", n.total.tests)
  li <- 1
  for (nsa in nsamps) {
    for (tp in true.pct) {
      for (st in sig.thresh) {
        for (eff in size.effect) {
          for (lr in low.hi.ratio) {
            total.tests[[li]] <- list()
            params <- list(nsa = nsa, tp = tp, st = st, eff = eff, ng = n.genes, nst = n.studies, lr = lr)
            print(simplify2array(params))
            total.tests[[li]]$params <- params
            generated <- make.power.counts(params, distros, specify.cols)
            actual.statuses <- lapply(generated$pc, function(x) x$actual.status)
            actual.sizes <- sapply(generated$pc, function(x) x$actual.size)
            write.power.counts(generated$pc, generated$afl, generated$ags, dirname)
            roster.ags <- get.roster.ags(dirname)
            # max.reads <- max.counts(generated$pc)
            max.reads <- max(roster.ags[, "reads"])
            dset <- make.dataset(dirname, 
              roster.ags, 
              impute.afl = T,
              nreads = max.reads, 
              skip.rarefy = skip.rarefy)
            dset$roster.ags <- roster.ags
            #print(head(roster.ags))
            print(actual.sizes)
            params$sizes <- actual.sizes
            if (cheatsize) { all.sizes <- actual.sizes } else { all.sizes <- NULL }
            print(all.sizes)
            test <- variance.test(dir = dirname, 
              pseud = 1,
              use.existing = dset,
              real.B = B,
              background.B = B,
              nb.sizes = all.sizes,
              nb.estimator = size.method, 
              pNodes = nodes, 
              skip.resample = !nb.only)
            if (troubleshoot) return(test)
            if (!nb.only) {
              pv <- pvals.from.vartest(test, sides = 2, nodes = nodes)
            } else {
              pv <- pvals.from.nb.only(test, sides = 2, nodes = nodes)
            }
            print(head(pv$all$pv))
            qv <- tryCatch(qvals(na.omit(pv$all$pv)), error = function(e) NA)
            total.tests[[li]]$pv <- pv
            total.tests[[li]]$qv <- qv
            total.tests[[li]]$actual.status <- actual.statuses
            if (savetest) total.tests[[li]]$test <- test
            total.tests[[li]]$counts <- test$dset$counts.mtx
            param.list[[li]] <- params
            li <- (li + 1)
          }
        }
      }
    }
  }
  return(list(tt = total.tests, par = param.list))
}

make.power.counts <- function(params, distros, specify.cols = NULL, min.dset = 2) {
  dset.names <- paste0("study", 1:params$nst)
  if (is.null(specify.cols) || (specify.cols == "uneven")) {
    samps.per.dset <- randomly.divide.samples(params$nsa, params$nst, min.dset)
  } else if (specify.cols == "even") {
    samps.per.dset <- evenly.divide.samples(params$nsa, params$nst, min.dset)
  } else {
    samps.per.dset <- specify.cols
  }
  names(samps.per.dset) <- dset.names
  pc <- (lapply(1:length(samps.per.dset), function(n) {
    ns <- samps.per.dset[n]
    means <- exp(rnorm(params$ng,
        mean = distros$means["mean"], sd = distros$means["sd"]))
    make.individual.dset(params, distros, ns, means, col.prefix = dset.names[n])
  }))
  names(pc) <- dset.names
  afl <- make.afl.distro(pc, distros)
  ags.ests <- exp(rnorm(params$nsa,
    mean = distros$ags["mean"],
    sd = distros$ags["sd"])) + 1
  return(list(pc = pc, afl = afl, ags = ags.ests))


}

write.power.counts <- function(pc, afl, ags, dirname) {
#  print(afl[[1]] %>% head)
  create.test.dir(dirname)
  roster <- make.roster(pc, dirname)
  ags <- make.ags(pc, ags.ests = ags)
  write.roster(roster, dirname)
  write.ags(ags, dirname)
  for (study.name in names(pc)) {
    for (samp in colnames(pc[[study.name]]$scaled.gs)) {
#      print(samp)
      write.counts(dirname, samp, pc[[study.name]]$scaled.gs[, samp], roster)
#      print(afl[[study.name]] %>% head)
      write.afl(dirname, samp, afl[[study.name]][, samp], roster)
    }
  }
  return(list(roster = roster,
      ags = ags,
      pc = pc,
      afl = afl))
}

randomly.divide.samples <- function(total, divisions, min.dset = 2) {
  dset.cols <- vector("numeric", divisions)
  for (d in 1:divisions) {
    max.dset <- (total - sum(na.omit(dset.cols))) -
      (min.dset * (divisions - d))
    if (d < divisions) {
      cols <- round(runif(1) * (max.dset - min.dset)) + min.dset
    } else {
      cols <- total - sum(dset.cols)
    }
    dset.cols[d] <- cols
  }
  return(dset.cols)
}
evenly.divide.samples <- function(total, divisions, min.dset = 2) {
  dset.cols <- vector("numeric", divisions)
  n.per.dset <- round(total / divisions)
  leftover <- total - ((divisions - 1) * n.per.dset)
  dset.cols[1:(divisions - 1)] <- n.per.dset
  dset.cols[divisions] <- leftover
  return(dset.cols)
}



# not sure what to do abt pseudocount, maybe just leave it
make.individual.dset <- function(params, distros, nc, means = NULL, col.prefix = "study0") {
  cols <- nc
  rows <- params$ng
  genes <- paste0("gene", 1:rows)
  sample.names <- paste0(col.prefix, "sample", 1:cols)
  eff <- params$eff
  # print(eff)
  if (length(eff) == 1) {
    low.eff <- eff
    high.eff <- eff
  } else {
    low.eff <- eff["low"]
    high.eff <- eff["high"]
  }
  base.size <- exp(rnorm(1, distros$sizes["mean"], distros$sizes["sd"]))
  if (is.null(means)) {
    means <- exp(rnorm(params$ng,
        mean = distros$means["mean"], sd = distros$means["sd"]))
    mean.fx <- rnorm(params$ng,
        mean = distros$dsets["mean"],
        sd = distros$dsets["sd"])
    means <- exp(log(means) + mean.fx) # integrate mean fx
  }
  #print(head(means))
  #readline()
  n.null <- ceiling((1 - params$tp) * rows)
  n.alt <- rows - n.null
  n.low <- ceiling((n.alt * params$lr) / (1 + params$lr))
  n.hi <- n.alt - n.low
  null.rn <- rnbinom((cols * n.null),
    mu = rep(means[1:n.null], each = cols), size = base.size)
  low.rn <- rnbinom((cols * n.low),
    mu = rep(means[(n.null+1):(n.null+n.low)], each = cols),
    size = base.size / low.eff)
  high.rn <- rnbinom((cols * n.hi),
    mu = rep(means[(n.null+n.low+1):rows], each = cols), 
    size = base.size * high.eff)
  dset <- matrix(nr = rows, nc = cols, c(null.rn, low.rn, high.rn), byrow = T)
  null.genes <- genes[1:n.null]
  low.genes <- genes[(1+n.null):(n.null+n.low)]
  high.genes <- genes[(1+n.null+n.low):(n.null+n.low+n.hi)]
  rownames(dset) <- genes
  colnames(dset) <- sample.names
  #print(apply(dset[1:5, ], 1, mean))
  return(list(scaled.gs = dset, actual.status = list(null = n.null, low = n.low, hi = n.hi, alt = n.alt,
        names = list(null = null.genes, low = low.genes, hi = high.genes)), actual.size = base.size))
}

make.afl.distro <- function(multi.study, distros) {
  afl <- lapply(multi.study, function(st) {
    afls <- exp(rnorm(nrow(st$scaled.gs) * ncol(st$scaled.gs),
      mean = distros$afl["mean"],
      sd = distros$afl["sd"])) + 1
    all.afls <- matrix(nr = nrow(st$scaled.gs),
      nc = ncol(st$scaled.gs), 
      afls)
    dimnames(all.afls) <- dimnames(st$scaled.gs)
    return(all.afls)
  })
}

excludefrom <- function(x, y) { x[setdiff(names(x), y)] }

calc.power <- function(pw, sig = 0.05) {
  ceil <- length(pw$tt)
  nulls <- sapply(pw$par, function(x) (1 - x$tp) * x$ng)
  n.alt <- sapply(pw$par, function(x) (x$tp) * x$ng)
  ng <- sapply(pw$par, function(x) x$ng)
  n.low <- sapply(pw$par, function(x) ceiling((x$tp * x$ng * x$lr) / (1 + x$lr)))
  # print(n.low)
  n.hi <- ng - n.low
  # print(n.hi)
  null.genes <- lapply(nulls, function(x) paste0("gene", 1:x))
  lo.genes <- lapply(1:length(n.low), function(x) paste0("gene", (nulls[x]+1):(n.low[x]+nulls[x]) ))
  hi.genes <- lapply(1:length(n.hi), function(x) paste0("gene", (nulls[x]+n.low[x]+1):ng[x] ))
  # sapply(lo.genes, function(x) print(head(x)))
  # sapply(hi.genes, function(x) print(head(x)))
  eff.size <- sapply(1:ceil, function(x) pw$tt[[x]]$params$eff)
  n.samps <- sapply(1:ceil, function(x) pw$tt[[x]]$params$nsa)
  tp <- sapply(1:ceil, function(x) pw$tt[[x]]$params$tp)
  power <- sapply(1:ceil, function(x) mean(excludefrom(pw$tt[[x]]$pv$all$pv, null.genes[[x]]) <= sig))
  alpha <- sapply(1:ceil, function(x) mean(na.omit(pw$tt[[x]]$pv$all$pv[null.genes[[x]]] <= sig)))
  power.hi <- sapply(1:ceil, function(x) mean(na.omit(pw$tt[[x]]$pv$all$pv[hi.genes[[x]]] <= sig)))
  power.lo <- sapply(1:ceil, function(x) mean(na.omit(pw$tt[[x]]$pv$all$pv[lo.genes[[x]]] <= sig)))
  empirical.fdr.05 <- sapply(1:ceil, function(x) { 
    #sigs  <- nw(qvalue(pw$tt[[x]]$pv$all$pv, pi0.method = "bootstrap")$qvalues <= sig)
    sigs  <- nw(p.adjust(pw$tt[[x]]$pv$all$pv, 'BH') <= sig)
    fdr <- length(intersect(sigs, null.genes[[x]])) / length(sigs)
    return(fdr) 
  })
  return(list(ng = null.genes, es = eff.size, ns = n.samps, power = power, alpha = alpha, tp = tp, fdr05 = empirical.fdr.05, hi.p = power.hi, lo.p = power.lo))
}

process.mtx <- function(x) {
  y <- apply(x[-1,], 1, function(z) Reduce(c, z))
  rownames(y)  <- colnames(x[-1])
  return(data.matrix(y))
}

make.power.mtx <- function(cp, coln, agg = median) {
  mtx <- data.frame(cbind(1:length(cp$es), cp$es, cp$ns, cp[[coln]]))
  colnames(mtx) <- c("id", "es", "ns", "value")
  tmp <- dcast(mtx, es ~ ns, value.var = colnames(mtx)[4], fun.aggregate = agg)
  return(tmp)
}

make.3d.power.plot <- function(pw) {
  ceil <- length(pw$tt)
  scatterplot3d(sapply(1:ceil, function(x) pw$tt[[x]]$params$eff),
    sapply(1:ceil, function(x) pw$tt[[x]]$params$nsa),
    sapply(1:ceil, function(x) mean(excludefrom(pw$tt[[x]]$pv$all$pv, pw$tt[[x]]$ng) <= 0.05)),
    type = 'h', xlab = 'effect size', ylab = 'no. samples', zlab = 'power', highlight.3d = T)
}

all.alpha <- function(pwr, sig = 0.05) {
  nsa <- sapply(pwr$tt, function(X) if (X$params$eff == 1) { X$params$nsa } else {NA})
  alpha <- sapply(pwr$tt, function(X) mean(X$pv$all$pv <= sig))
  return(cbind(nsa, alpha))
}

mc <- function(x) x - mean(x)

get.long <- function(mtx) {
  wide = cbind(genes = rownames(mtx), data.frame(mtx))
  long = gather(wide, key = "genes")
  return(long)
}

overall.fit.mle <- function(mtx, init = NULL) {
  if (is.null(init)) { init = fit.shrunk.nb.moments(mtx) }
  long <- get.long(mtx)
  mu.init <- apply(mtx, 1, mean)
  glm.nb(value ~ genes, data = long, init.theta = init, link = log, trace = 3, control = glm.control(maxit = 100))$theta
}

hybrid.mle <- function(mtx) {
  m.sizes <- apply(mtx, 1, function(x) get.nb.size.moments(x, const = "size"))
  middle <- quantile(na.omit(m.sizes), c(0.4, 0.6))
  middle.mtx <- mtx[which((m.sizes >= middle[1]) & (m.sizes <= middle[2])),]
  n.middle <- dim(middle.mtx)[1]
  to.sample <- min(n.middle, 100)
  overall.fit.mle(middle.mtx[sample(1:n.middle, to.sample), ])
}

default.params <- list(nsa = 35, tp = 0.1, st = 0.05, eff = 2, ng = 1000, nst = 3, lr = 1)
null.params <- list(nsa = 67, tp = 0.1, st = 0.05, eff = 1, ng = 1000, nst = 3, lr = 1)
big.params <- list(nsa = 67, tp = 0.1, st = 0.1, eff = 8, ng = 1000, nst = 3, lr = 1)
biggest.params <- list(nsa = 120, tp = 0.33, st = 0.1, eff = 8, ng = 1000, nst = 3, lr = 1)
biggest.asym.params.1 <- list(nsa = 120, tp = 0.465, st = 0.1, eff = 8, ng = 1000, nst = 3, lr = 6.7)
biggest.asym.params.2 <- list(nsa = 120, tp = 0.465, st = 0.1, eff = 8, ng = 1000, nst = 3, lr = .15)
default.distros <- list(means = c(mean = 2.94, sd = 2.23),
    dsets = c(mean = -0.49, sd = 0.78),
    sizes = c(mean = 0.529, sd = 0.495),
    ags = c(mean = 15.0, sd = 1e-5),
    afl = c(mean = 7, sd = 0.01))
small.distros <- list(means = c(mean = 2.94, sd = 2.23),
    dsets = c(mean = -0.49, sd = 0.78),
    sizes = c(mean = -0.65, sd = 0.57),
    ags = c(mean = 15.0, sd = 1e-5),
    afl = c(mean = 7, sd = 0.01))

default.params <- list(nsa = 35, tp = 0.44, st = 0.05, eff = 3, ng = 1000, nst = 3, lr = 1)

power.count.wrapper <- function(params, distros, min.d = 1, divide = NULL, tmpdir = "tmp_dset") {
  pc <- make.power.counts(params, distros, min.dset = min.d, specify.cols = divide)
  size <- sapply(pc$pc, function(x) x$actual.size)
  write.power.counts(pc$pc, pc$afl, pc$ags, tmpdir)
  roster.ags <- get.roster.ags(tmpdir)
  #max.reads <- max.counts(pc$pc)
  max.reads <- max(roster.ags[, "reads"])
  print(max.reads)
  dset <- make.dataset(tmpdir, 
    roster.ags, 
    impute.afl = T,
    nreads = max.reads,
    skip.rarefy = T)
  dset$roster.ags <- roster.ags
  dset$size <- size
  dset$nreads <- max.reads
  dset$answers <- pc$pc$study1$actual.status$names
  return(dset)
}
