#!/usr/bin/python

import os, sys, glob, re
get_nreads = re.compile("total reads output: (.*)")

sample_dirs = [x for x in glob.glob("./output/*") if os.path.isdir(x)]
samples = [os.path.basename(x) for x in sample_dirs]
sample_to_dir = dict(zip(samples, sample_dirs))

sample_to_study = dict()
sample_to_study_file = "sample-to-study.tab"
with open(sample_to_study_file, 'rb') as fh:
  for line in fh:
    row = line[:-1].split("\t")
    sample_to_study[row[0]] = row[1]


roster_file = "roster.tab"
if os.path.exists(roster_file):
  raise IOError("%s exists" % roster_file)
with open(roster_file, 'w') as fh:
  print >> fh, "\t".join(["sample_id",
    "counts_file",
    "afl_file",
    "study",
    "species_tags_file",
    "reads"])
  for s in samples:
    d = sample_to_dir[s]
    counts_file = os.path.abspath(os.path.join(d, "%s.counts" % s))
    afl_file = os.path.abspath(os.path.join(d, "%s.afl" % s))
    spectag_file = os.path.abspath(os.path.join(d, "%s.spectag" % s))
    skip = False
    for f in [counts_file, afl_file]:
      if not os.path.exists(f):
        print >> sys.stderr, "counts/afl file missing for %s" % s
        skip = True
    if skip:
      continue
    log_file = os.path.join(d, "%s.log" % s)
    reads = -1
    try:
      with open(log_file, 'rb') as lh:
        for line in lh:
          text = line[:-1]
          reads_line = re.search(get_nreads, text)
          if reads_line is not None:
            reads = int(reads_line.group(1))
    except IOError:
      print >> sys.stderr, "log file not found or couldn't be read: %s" % log_file
      continue
    if reads < 1:
      continue
    study = sample_to_study[s]
    print >> fh, "\t".join([s, counts_file, afl_file, study, spectag_file, str(reads)])

ags_file = "ags.tab"
if os.path.exists(ags_file):
  raise IOError("%s exists" % ags_file)
with open(ags_file, 'w') as fh:
  print >> fh, "\t".join(["proj",
    "sample_id",
    "reads_sampled",
    "read_length",
    "avg_size"])
  for s in samples:
    d = sample_to_dir[s]
    ags_file = os.path.join(d, "%s.ags" % s)
    proj = sample_to_study[s]
    try:
      with open(ags_file, 'rb') as ah:
        header = ah.readline()[:-1].split("\t")
        row = ah.readline()[:-1].split("\t")
        ags_dict = dict(zip(header, row))
    except IOError:
      print >> sys.stderr, "ags file not found or couldn't be read: %s" % ags_file
      continue
    reads = ags_dict["reads"]
    read_length = str(float(ags_dict["bp"]) / int(ags_dict["reads"]))
    avg_size = ags_dict["ags"]
    print >> fh, "\t".join([proj, s, reads, read_length, avg_size])

