source("variance-analyze-functions.R")
source("variance-analyze-tests.R")

# Make test data under the directory "example-data"
test.data <- make.test.data.files("example-data", ns = 3)

# Get the number of reads we want to rarefy to (here, just taking the max)
nreads <- test.data$roster[,"reads"] %>% max %>% as.numeric

# Run the test. Note: change pNodes to the number of cores you would like to
# devote to this process
test.mode.results <- test.wrapper("example-data", background.B = 250,
  skip.resample = T, nb.estimator = fit.shrunk.nb.moments.mode.log,
  nreads = nreads, verbose = TRUE, pNodes = 4)

# Calculate significant hits
test.sigs <- make.sigs(overall.results$pvals)

# Repeat for a dataset where there are some expected true positives

default.distros <- list(means = c(mean = 2.94, sd = 2.23),
    dsets = c(mean = -0.49, sd = 0.78),
    sizes = c(mean = 0.529, sd = 0.495),
    ags = c(mean = 15.0, sd = 1e-5),
    afl = c(mean = 7, sd = 0.01))
nnt.data <- power.count.wrapper(params = list(nsa = 120, tp =0.3, st = 0.05, eff = 3, ng = 1000, nst = 3, lr = 1), distros = default.distros, tmpdir = "non-null-example-data")
test2.results <- test.wrapper("non-null-example-data", background.B = 250, skip.resample = T, nb.estimator = fit.shrunk.nb.moments.mode.log, nreads= nnt.data$nreads, verbose = TRUE, pNodes = 4)
test2.sigs <- make.sigs(test2.results$pvals)
