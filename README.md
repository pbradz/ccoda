# CCoDA #
## Covariate-Corrected Dispersion Analysis ##
![ccoda.png](https://bitbucket.org/repo/xGkqxn/images/953930390-ccoda.png)

### What is this repository for? ###

* This repository contains code used to perform a test to identify significantly (in)variable gene families from count-based sequence data, as well as code used to perform downstream analyses and generate accompanying figures.
* The test is described in "Proteobacteria explain significant functional variability in the human gut microbiome", Bradley & Pollard (forthcoming, pre-print available at [bioRxiv](http://biorxiv.org/content/early/2016/10/13/056614)).

### Main functions ###

 * **The test itself**
     * **variance-analyze-functions.R**: functions for reading data, performing the test, and calculating significance
          * main function is `test.wrapper()`, or `variance.test()` if you don't need to calculate p-values
          * returns a list with the following values:
               * roster.ags: Table giving paths to input data files, plus average genome sizes
               * dset: list giving count matrices ("counts"), average family length ("afl"), an overall count matrix ("counts.mtx"), and roster.ags
               * vtest: list giving results of variance test
                  * dset (see above)
                  * norm.data (list giving data after normalizing for AGS/AFL)
                  * fake.dset (statistics generated under the negative binomial null model)
                  * real.resample: list with the following elements
                     * initial: output of fit.lite.model to list of datasets
                     * actual: real test statistics for each gene
                     * resampled: values obtained by repeating the test, resampling from columns with replacement
                  * pvals: list containing p-values and effect directions
                  * resid.log.rpkg: residuals from fitting model to real data, in units of residual log RPKG
     * **generate-figure-functions.R**: functions used to perform analyses for manuscript and generate figures (see, e.g., `Figure.Megamodules()`)
     * **generate-figure-scripts.R**: calls to functions in generate-figure-functions.R that were used to generate figures
 * **Phylogenetic distribution**
     * **taxonomy/tree-heights.R**: script to calculate and output PD from protein family tree files
     * **taxonomy/lca-predict.R**: script to impute PD for genes with few representatives
     * **taxonomy/lca-restrict.py**: script to parse NCBI taxonomy tree and read protein family trees, then output the taxonomic rank of the LCA for those trees
     * **taxonomy/kegg-ids-under-top-level.py**: script to parse NCBI taxonomy tree and KEGG information, then output all KEGG ids under a certain node in the tree (e.g., Bacteria or Archaea)
     * **taxonomy/data/*.format**: first few rows of files used by lca-restrict.py, kegg-ids-under-top-level.py
 


### Who do I talk to? ###

* Patrick Bradley: pbradz@pbradz.org (Pollard Lab, Gladstone Institutes)

### License ###

Code is copyright (c) 2016--2017 Patrick Bradley and released under the MIT license (as follows):

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.